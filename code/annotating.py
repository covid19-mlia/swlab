import bisect
import logging
from okgraph.core import OKgraph
from okgraph.utils import logger
import os
from os import path
from preprocessing import name_unified_corpus, INDEX_FOLDER, TEST_DATA_DIR, TRAINING_DATA_DIR, PROCESSED_DATA_DIR, EXCLUDED_NAMES
from indexing import CorpusIndex, FIELD_CORPUS, FIELD_PHRASE_CONTENT, FIELD_PHRASE_POS
import re


def main():
    clear_annotations()
    annotations = []
    annotations = get_annotation_set_expansion(annotations)
    print('Writing annotations')
    previous_ann_path = ''
    i = 1
    for annotation in annotations:
        if annotation.ann_path != previous_ann_path:
            i = 1
        else:
            i = i+1
        previous_ann_path = annotation.ann_path
        with open(annotation.ann_path, 'a', encoding='utf-8') as f_ann:
            f_ann.write(f'T{i}\t{annotation}\n')

def get_annotation_set_expansion(annotations):
    expansion_data = {
        'en': {
            'drug': {
                'seed': ['improvac', 'pemetrexed_lilly', 'protopic'],
                'category': 'drug-trt',
                'k': 200
            },
            'sosy': {
                'seed': ['breathing_difficulty', 'disorientation', 'blindness'],
                'category': 'sosy-dis',
                'k': 200
            },
            'dis': {
                'seed':  ['tardive_dyskinesia', 'diabetes_mellitus', 'cardiomyopathy'],
                'category': 'sosy-dis',
                'k': 200
            },
            'test': {
                'seed': ['screening', 'ct_scan', 'mammography'],
                'category': 'tests',
                'k': 200
            }
        },
        'it': {
            'drug': {
                'seed': ['improvac', 'pemetrexed_lilly', 'protopic'],
                'category': 'drug-trt',
                'k': 200
            },
            'sosy': {
                'seed': ['respirazione_difficoltosa', 'disorientamento', 'cecità'],
                'category': 'sosy-dis',
                'k': 200
            },
            'dis': {
                'seed':  ['discinesia_tardiva', 'diabete_mellito', 'cardiomiopatia'],
                'category': 'sosy-dis',
                'k': 200
            },
            'test': {
                'seed': ['screening', 'tomografia_computerizzata', 'mammografia'],
                'category': 'tests',
                'k': 200
            }
        },
    }

    data_dir_languages = os.listdir(TRAINING_DATA_DIR)
    for language_folder in data_dir_languages:
        unified_corpus_name = name_unified_corpus(language_folder)
        unified_corpus = unified_corpus_name + '.txt'
        unified_corpus_dir = path.join(PROCESSED_DATA_DIR, language_folder, unified_corpus_name)
        unified_corpus_path = path.join(unified_corpus_dir, unified_corpus)

        logger.setLevel(logging.ERROR)
        okg = OKgraph(corpus_file=unified_corpus_path)
        logger.setLevel(logging.ERROR)

        index_path = path.join(PROCESSED_DATA_DIR, language_folder, INDEX_FOLDER)
        ix = CorpusIndex(index_path)

        set_expansion_algo = 'centroid_boost'
        set_expansion_options = {
            'embeddings': okg.embeddings,
            'step': 20,
            'fast': True
        }
        for sub_category_key in expansion_data.get(language_folder).keys():
            print(f'Searching for {language_folder}-{sub_category_key}')
            sub_category_data = expansion_data[language_folder][sub_category_key]
            seed = sub_category_data['seed']
            category = sub_category_data['category']
            k = sub_category_data['k']
            expansion = okg.set_expansion(seed, k, set_expansion_algo, set_expansion_options)
            sub_category_elements = seed + expansion
            annotations = expand_annotations(annotations, ix, sub_category_elements, category)

    return annotations


def expand_annotations(annotations, ix, to_search_elements, e_category):
    for e in to_search_elements:
        print(f'\tSearching for {e}')
        word = e.replace('_', ' ').lower()
        query_results = ix.search_word(word)
        for result in query_results:
            corpus_path = result[FIELD_CORPUS]
            corpus_dir = os.path.dirname(corpus_path)
            corpus_name, _ = os.path.splitext(os.path.basename(corpus_path))
            corpus_ann_path = path.join(corpus_dir, corpus_name + ".ann")

            phrase = result[FIELD_PHRASE_CONTENT]
            pos = result[FIELD_PHRASE_POS]
            for match in re.finditer(word, phrase.lower()):
                start = match.start()
                end = match.end()
                matched_word = phrase[start:end]
                annotation = Annotation(corpus_ann_path, e_category, pos + start, pos + end, matched_word)
                bisect.insort(annotations, annotation)
    return annotations


def clear_annotations():
    training_dir_folders = os.listdir(TRAINING_DATA_DIR)
    for language_folder in training_dir_folders:
        language_path = path.join(TRAINING_DATA_DIR, language_folder)
        for file in os.listdir(language_path):
            file_name, file_extension = path.splitext(file)
            if file_extension == '.txt' and file_name not in EXCLUDED_NAMES:
                ann_file_path = path.join(language_path, file_name+'.ann')
                with open(ann_file_path, 'w', encoding='utf-8'):
                    pass
    test_dir_folders = os.listdir(TEST_DATA_DIR)
    for language_folder in test_dir_folders:
        language_path = path.join(TEST_DATA_DIR, language_folder)
        for file in os.listdir(language_path):
            file_name, file_extension = path.splitext(file)
            if file_extension == '.ann' and file_name not in EXCLUDED_NAMES:
                ann_file_path = path.join(language_path, file_name+'.ann')
                with open(ann_file_path, 'w', encoding='utf-8'):
                    pass


class Annotation:
    ann_path: str
    category: str
    start_pos: int
    end_pos: int
    word: str

    def __init__(self, ann_path, category, start_pos, end_pos, word):
        self.ann_path = ann_path
        self.category = category
        self.start_pos = start_pos
        self.end_pos = end_pos
        self.word = word

    def __lt__(self, other):
        if self.ann_path < other.ann_path:
            return True
        elif self.ann_path == other.ann_path:
            if self.start_pos < other.start_pos:
                return True
            else:
                return False
        else:
            return False

    def __str__(self):
        return f'{self.category} {self.start_pos} {self.end_pos}\t{self.word}'


if __name__ == '__main__':
    main()
