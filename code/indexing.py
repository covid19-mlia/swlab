import os
import shutil
from whoosh import index
from whoosh.fields import Schema, NUMERIC, TEXT
from whoosh.index import FileIndex
from whoosh.qparser import QueryParser

FIELD_CORPUS = 'corpus'
FIELD_PHRASE_NUM = 'phrase_num'
FIELD_PHRASE_CONTENT = 'phrase_content'
FIELD_PHRASE_POS = 'phrase_pos'


class CorpusIndex:
    index_path: str
    schema = Schema(
        corpus=TEXT(stored=True),
        phrase_num=NUMERIC(int, 64, signed=False, stored=True),
        phrase_content=TEXT(stored=True),
        phrase_pos=NUMERIC(int, 64, signed=False, stored=True)
    )
    ix: FileIndex


    def __init__(self, index_path):
        self.index_path = index_path


    def create_index(self):
        # cleanup
        shutil.rmtree(self.index_path, ignore_errors=True)
        os.makedirs(self.index_path)
        # actual index creation
        self.ix = index.create_in(self.index_path, self.schema)


    def index_corpus(self, corpus_path):
        c_phrases = 0
        c_chars = 0
        writer = self.ix.writer()
        i = 0
        for phrase in self._get_phrases(corpus_path):
            writer.add_document(
                corpus=corpus_path,
                phrase_num=c_phrases,
                phrase_content=phrase,
                phrase_pos=c_chars
            )
            print(f'\r- Indexing phrase number {i}', end='')
            c_phrases += 1
            c_chars += len(phrase)
            i += 1
        print(f'\r- Committing index', end='')
        writer.commit()
        print(f'\r', end='')


    def search_word(self, word):
        ix = index.open_dir(self.index_path)
        query = QueryParser(FIELD_PHRASE_CONTENT, self.schema).parse(word)
        list_results = []
        with ix.searcher() as searcher:
            query_results = searcher.search(query)
            for result in query_results:
                list_results += [{
                    FIELD_CORPUS: result[FIELD_CORPUS],
                    FIELD_PHRASE_NUM: result[FIELD_PHRASE_NUM],
                    FIELD_PHRASE_CONTENT: result[FIELD_PHRASE_CONTENT],
                    FIELD_PHRASE_POS: result[FIELD_PHRASE_POS]
                }]
        return list_results

    @staticmethod
    def _get_phrases(corpus_path):
        with open(corpus_path, 'r', encoding="utf-8") as f:
            for line in f:
                yield line
