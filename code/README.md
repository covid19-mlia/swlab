# Dependencies
This code makes use of the OKgraph library https://github.com/atzori/okgraph  
Plaintext should be extracted from Wikipedia dumps by using WikiExtractor https://github.com/attardi/wikiextractor

# File input
These folders are expected to be found in the folder structure:
## okgraph
The OKgraph library would reside in this folder
## dataset
### test
Text files with the test corpus, divided in folders by language
### training
Text files with the training corpus, divided in folders by language
### processed
This folder will be used by our code to save the indexes and the Gensim model, divided in folders by language
## WIKIen / WIKIit / WIKIxx
Should be the output folders for WikiExtractor

# Execution
The scripts should be run in this order:

1. preprocessing.py  
    Will take care of generating the model from corpora
2. annotating.py  
    Will generate annotation files
# File output
Annotation files will be saved in the same folders as their source text files, with matching names