from indexing import CorpusIndex
import logging
from okgraph.core import OKgraph
from okgraph.utils import logger
import os
from os import path
import pathlib
import re
import shutil
import time

PROCESSED_DATA_DIR = path.join('dataset', 'processed')
TEST_DATA_DIR = path.join('dataset', 'test')
TRAINING_DATA_DIR = path.join('dataset', 'training')
INDEX_FOLDER = 'indexdir'
INCLUDED_EXTENSION = ['.txt']
EXCLUDED_NAMES = ['sample']
INCLUDE_TEST_IN_UNIFIED_CORPUS = True#False


def main():
    # cleanup from previous runs
    shutil.rmtree(PROCESSED_DATA_DIR, ignore_errors=True)

    # one per language
    language_folders = os.listdir(TRAINING_DATA_DIR)

    for language_folder in language_folders:
        training_language_dir = path.join(TRAINING_DATA_DIR, language_folder)
        test_language_dir = path.join(TEST_DATA_DIR, language_folder)

        print(f'Processing {training_language_dir}')

        unified_corpus_name = name_unified_corpus(language_folder)
        unified_corpus = unified_corpus_name + '.txt'
        unified_corpus_dir = path.join(PROCESSED_DATA_DIR, language_folder, unified_corpus_name)
        # creating directory '<PROCESSED_DATA_DIR>/<LANG>/unified_<LANG>_corpus/'
        pathlib.Path(unified_corpus_dir).mkdir(parents=True, exist_ok=True)
        # path unified_<LANG>_corpus.txt file
        unified_corpus_path = path.join(unified_corpus_dir, unified_corpus)

        # creating whoosh index
        index_path = path.join(PROCESSED_DATA_DIR, language_folder, INDEX_FOLDER)
        corpus_index = CorpusIndex(index_path)
        corpus_index.create_index()

        print(f"Creating unified corpus and index from test and training data")

        # corpus is splitted in various files
        for file in os.listdir(training_language_dir):

            file_name, file_extension = path.splitext(file)
            if file_extension not in INCLUDED_EXTENSION or file_name in EXCLUDED_NAMES:
                continue

            r_corpus_path = path.join(training_language_dir, file)
            print(f'\tReading {r_corpus_path}')

            with open(r_corpus_path, 'r', encoding='utf-8') as fr_corpus, \
                 open(unified_corpus_path, 'a', encoding='utf-8') as fw_unified_corpus:

                text = fr_corpus.read()
                cleaned_text = text_preprocessing(text)
                
                print(f'\t\tWriting on {unified_corpus_path}')
                fw_unified_corpus.write(cleaned_text)
            print(f'\t\tIndexing {path.join(r_corpus_path)}')
            corpus_index.index_corpus(r_corpus_path)
#
        for file in os.listdir(test_language_dir):

            file_name, file_extension = path.splitext(file)
            if file_extension not in INCLUDED_EXTENSION or file_name in EXCLUDED_NAMES:
                continue

            r_corpus_path = path.join(test_language_dir, file)
            if INCLUDE_TEST_IN_UNIFIED_CORPUS:
                print(f'\tReading {r_corpus_path}')

                with open(r_corpus_path, 'r', encoding='utf-8') as fr_corpus, \
                     open(unified_corpus_path, 'a', encoding='utf-8') as fw_unified_corpus:

                    text = fr_corpus.read()
                    cleaned_text = text_preprocessing(text)

                    print(f'\t\tWriting on {unified_corpus_path}')
                    fw_unified_corpus.write(cleaned_text)
            print(f'\t\tIndexing {path.join(r_corpus_path)}')
            corpus_index.index_corpus(r_corpus_path)

        print(f'\tCreated {unified_corpus_path}')

        # UPDATE: append WikiExtractor output here, so that it is used for set expansion, but not in index
        print(f'Adding Wikipedia text to {unified_corpus_path}')
        append_wikipedia(language_folder, unified_corpus_path)
        ##END UPDATE##

        print(f'\tCreating {unified_corpus_path} resources')
        time.sleep(1)
        logger.setLevel(logging.INFO)
        okg = OKgraph(corpus_file=unified_corpus_path)
        logger.setLevel(logging.ERROR)
        time.sleep(1)
		
        print()
		
        corpus_dict_path = path.join(unified_corpus_dir, unified_corpus_name + '_dict.txt')
        print(f'Saving embeddings dictionary in {corpus_dict_path}')
        with open(corpus_dict_path, 'w', encoding='utf-8') as f_dict:
            for w in okg.embeddings.model:
                f_dict.write(w[0]+'\n')


def text_preprocessing(text):
    cleaned_text = text + "\n"
    ##cleaned_text = re.sub(r"[\n\t']", " ", cleaned_text)
    cleaned_text = re.sub(r"[\t']", " ", cleaned_text)###
    cleaned_text = re.sub(r"\n+", "\n", cleaned_text)###
    ##cleaned_text = re.sub(r"[^\w\s]", "", cleaned_text)
    cleaned_text = re.sub(r"[^\w\s\n]", "", cleaned_text)###
    ##cleaned_text = re.sub(r"[\s]+", " ", cleaned_text)
    cleaned_text = re.sub(r"[ \t\v]+", " ", cleaned_text)###
    cleaned_text = re.sub(r"^\s", "", cleaned_text)
    cleaned_text = cleaned_text.lower()
    return cleaned_text


def name_unified_corpus(language):
    return f'unified_{language}_corpus'


# UPDATE
def append_wikipedia(language_folder, unified_corpus_path) :
    with open(unified_corpus_path, 'a', encoding='utf-8') as fw_unified_corpus :
        WIKI_DIR = 'WIKI' + language_folder
        wikiSubdirs = os.listdir(WIKI_DIR)
        subdirsLimit = 10   # OKgraph/Gensim seems to struggle with a huge unified corpus
        dirsToRead = wikiSubdirs[:subdirsLimit]

        for (count, subFolderName) in enumerate(dirsToRead, 1) :
            print(f'\tAppending from folder {count} / {subdirsLimit} / {len(wikiSubdirs)}', end='\r')

            subFolderPath = path.join(WIKI_DIR, subFolderName)
            for file in os.listdir(subFolderPath) :

                with open(path.join(subFolderPath, file), 'r', encoding='utf-8') as fr_corpus :
                    text = fr_corpus.read()
                    cleanedText = remove_wiki_tags(text)
                    fw_unified_corpus.write(cleanedText)
        print()

# Not doing it once and for all directly on the original files. TODO.
def remove_wiki_tags(text) :
    cleaned_text = text + " "
    cleaned_text = re.sub(r"^<doc[^\n]+\n.*\n", "", cleaned_text, flags=re.MULTILINE)
    cleaned_text = re.sub(r"</doc>\n", "", cleaned_text)
    cleaned_text = re.sub(r"\n\n+", "\n", cleaned_text)
    return text_preprocessing(cleaned_text)
##END UPDATE##


if __name__ == '__main__':
    main()
